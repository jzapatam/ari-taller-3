# README #

Sources y entregables del Taller 3, materia Alamacenamiento y Recuperacion de la Informacion.
Maestria en Ciencia de los Datos
Universidad EAFIT
Mayo 2020

### Integrantes ###

* Diana Carolina Benjumea
* Juan David Botero
* Juan David Zapata

### Enunciado del taller ###

* https://drive.google.com/file/d/1wX4nao92lYWtyoXitwORmEQ1H_HXlROi/view?usp=sharing

### Contenido ###

* TODO: incluir las ubicaciones dentros del repo de los files y notebooks e informe final del taller
* Codigos de ejemplo para desarrollar las actividades del Taller 3
    - examples/
        - extract-pdf-data-and-tokenize-it.ipynb : codigo de ejemplo para extraer data de un pdf y tokenizarlo. Lo hace 
          leyendo la columna "pdf_json_files" la data del csv sources/sample_metadata.csv, carga ese json y extrae toda
          la informacion del pdf y lo tokeniza.
        - Taller 3 - ST1800-003 ARI.ipynb : Ejemplo tokenizando el titulo y paso a paso de lo que vimos en la asesoría 
          del taller de la ultima clase
        - Task1-Solution .ipynb : solución tratando de resolver una de las tareas enunciadas en el reto de CORD-19 de 
          Kaggle :  Resuelve la pregunta"What is known about transmission, incubation, and environmental stability?"
        - Example-abstract.ipynb : Analisis, clasificación y detección exploratorio del topico "abstract" de los 
          metadatos   
